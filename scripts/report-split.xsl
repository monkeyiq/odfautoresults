<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:t="http://www.example.org/documenttests" >
<xsl:output method="xml"/>

    <xsl:param name="target"  />


    <xsl:template match="/t:documenttestsreport">
        <xsl:copy>
             <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="t:input">
       <xsl:copy-of select="."/>
    </xsl:template>

    
    <xsl:template name="copyout">
      <xsl:param name="selector" />
       <xsl:copy-of select="$selector"/>
    </xsl:template>
    <xsl:template match="t:target">
    </xsl:template>

    <xsl:template match="t:testreport">
      <xsl:copy>
	<xsl:copy-of select="@*"/><!--copy of all attributes-->
	
	<xsl:for-each select="t:input">
	  <xsl:copy-of select="."/>
	</xsl:for-each>
	<xsl:call-template name="copyout">
	  <xsl:with-param name="selector" select="t:target[@name=$target]" />
	</xsl:call-template>
      </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
