#!/bin/bash

BATCHNAME=${1:?Supply the location for the results as arg1. eg, /tmp/odftesting-2015nov};
export BATCHNAME

./run-odfautotests.sh "$BATCHNAME"
./run-postprocess.sh  "$BATCHNAME"

