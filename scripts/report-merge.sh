#!/bin/bash

SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
reporta=${1:?first report to merge};
reportb=${2:?second report to merge};

reporta="$(realpath $reporta)"
reportb="$(realpath $reportb)"

xmlstarlet tr "$SCRIPTDIR/report-merge.xsl"  \
	   -s secondreport="$reportb"        \
	   $reporta | xmllint --format - 
