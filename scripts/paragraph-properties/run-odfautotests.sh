#!/bin/bash
#


TESTCONFIGXML="paragraph-properties-odt.xml"
BATCHNAME=${1:?Supply the location for the results as arg1. eg, /tmp/odftesting-2015nov};


export BATCHNAME
export TESTFILE="$ODFAUTOTESTS/tests/$TESTCONFIGXML"
export SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

../support/process.sh
