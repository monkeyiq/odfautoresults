#!/bin/bash

for if in $(find . -name report.html); do
    echo $if
#    sed -i 's/href="[^\]*\\/href="/g' "$if"
    sed -i 's|href="[^/]*/output/|href="output/|g' "$if"
    sed -i 's|src="[^/]*/output/|src="output/|g' "$if"
    sed -i 's.\\./.g' "$if"
done
