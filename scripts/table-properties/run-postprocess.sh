#!/bin/bash
#

BATCHNAME=${1:?Supply the location for the results as arg1. eg, /tmp/odftesting-2015nov};
export BATCHNAME
export SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

../support/generate-merged-report-from-subparts.sh
