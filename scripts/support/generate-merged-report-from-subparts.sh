#!/bin/bash

OURSCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

REPORTMERGE="$OURSCRIPTDIR/../report-merge.sh"
report2htmlXSL="$ODFAUTOTESTS/report2html.xsl"

OUTPUTPATH="$BATCHNAME/$(basename $SCRIPTDIR)/results"
cd "$OUTPUTPATH"

echo "merging reports.xml files from $(pwd)"
REPORTS=$(find . -path "./report.xml" -prune -o -name "report.xml" -print|sort);
V=1
for r in $REPORTS; do
    if [ y$V = y1 ]; then
	V=0
	echo "first report $r..."
	cp "$r" /tmp/report.xml
    else
	echo "next report $r..."
	$REPORTMERGE "$r" /tmp/report.xml >| /tmp/reportB.xml
	mv -f /tmp/reportB.xml /tmp/report.xml
    fi
done

cp -avf /tmp/report.xml .
xmlstarlet tr $report2htmlXSL -s expanded=1 report.xml >| report.html

if=report.html
sed -i 's.\\./.g' "$if"
sed -i 's|href="[^/]*/output/|href="output/|g' "$if"
sed -i 's|src="[^/]*/output/|src="output/|g' "$if"
sed -i 's|src="results/|src="output/|g' "$if"
sed -i 's|href="results/|src="output/|g' "$if"


rsync -av report.html report.xml ../

echo "Copying and treating the source ODF files for Web inspection."
mkdir -p ../input
mkdir -p ../output
rsync -a ./config-calligra.xml/input  ..
for if in $(find . -name output); do
    rsync -a $if ..
done

cd ../output
$OURSCRIPTDIR/../expand-odt.sh . >/dev/null
cd ../input
$OURSCRIPTDIR/../expand-odt.sh . >/dev/null



