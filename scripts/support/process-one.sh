#!/bin/bash
#
#


if [ -z "$ODFAUTOTESTS" -o -z "$ODFTESTER" -o -z "$BATCHNAME" -o -z "$SCRIPTDIR" ]; then
    echo "Your environment is not setup completely!"
    exit 1;
fi
if [ -z "$TESTFILE" ]; then
    echo "No driver xml test file specified!";
    echo "Please ensure that TESTFILE is set";
    exit 1;
fi

CONFIGFILE=${1:?supply config path (relative to config dir) as arg1};



CFGDIR="$SCRIPTDIR/../configs/"
OUTPUTPATH="$BATCHNAME/$(basename $SCRIPTDIR)/results"
mkdir -p "$OUTPUTPATH"
cd "$OUTPUTPATH"
echo $CONFIGFILE
mkdir -p $CONFIGFILE
cd $CONFIGFILE

echo "Running $CONFIGFILE test in $(pwd)"
java -jar "$ODFTESTER" \
     -c "$CFGDIR/$CONFIGFILE" \
     -i ./input \
     -r ./output \
     -t $TESTFILE

