#!/bin/bash

OURSCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

function startDummyLibreOfficeConversion {
    cfg=$1
    echo $cfg
    CONFIGPATH="$OURSCRIPTDIR/../configs/$cfg"
    cmd=$(grep "command exe" "$CONFIGPATH" | head -1 | sed 's/.*"\([^"]*\)".*/\1/g')
    tool=$(grep -A 1 tool "$CONFIGPATH"    | tail -1 | sed 's/.*"\([^"]*\)".*/\1/g')
    echo $cmd
    if [[ $cmd == "/"*"factory.py" ]]; then
	echo "command seems sane... tool: $tool"
	killall -15 soffice.bin
	sleep 4
	cp -avf "$OURSCRIPTDIR/empty.odt" /tmp/
	$cmd --tool $tool -i /tmp/empty.odt -o /tmp/output.odt
    fi
}

# startDummyLibreOfficeConversion officeshots/odfautotests-libreoffice5-config.xml
# exit


if [[ $ODFSKIPTEST != *"calligra"* ]]; then
    $OURSCRIPTDIR/process-one.sh config-calligra.xml
fi
if [[ $ODFSKIPTEST != *"abiword"* ]]; then
    $OURSCRIPTDIR/process-one.sh officeshots/odfautotests-abiword-config.xml
fi
if [[ $ODFSKIPTEST != *"lo4"* ]]; then
    killall -15 soffice.bin
    sleep 4
    $OURSCRIPTDIR/process-one.sh officeshots/odfautotests-libreoffice4-config.xml
fi
if [[ $ODFSKIPTEST != *"lo5"* ]]; then
    killall -15 soffice.bin
    sleep 4
    $OURSCRIPTDIR/process-one.sh officeshots/odfautotests-libreoffice5-config.xml
fi
if [[ $ODFSKIPTEST != *"webodf"* ]]; then
    $OURSCRIPTDIR/process-one.sh officeshots/odfautotests-webodf-config.xml
fi
if [[ $ODFSKIPTEST != *"google"* ]]; then
    $OURSCRIPTDIR/process-one.sh officeshots/odfautotests-google-config.xml
fi
