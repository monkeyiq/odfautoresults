#!/bin/bash

REPORTMERGE=/Develop/officeshots/odfautoresults/report-merge.sh
report2htmlXSL=/Develop/officeshots/odfautotests/odfautotests/report2html.xsl

rm -f /tmp/report.xml
cp ./msoffice2013/report.xml  /tmp/report.xml
$REPORTMERGE ./word-and-lo4/report.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml
#$REPORTMERGE  ./local-abiword-lo5-webodf/report.xml /tmp/report.xml >| /tmp/reportB.xml
#mv -f /tmp/reportB.xml /tmp/report.xml
$REPORTMERGE  ./local-abiword-lo5-webodf/report-lo5.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml
$REPORTMERGE  ./local-webodfnew/report.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml
$REPORTMERGE  ./local-abiword-lo5-webodf/report-abiword.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml
$REPORTMERGE ./google/report.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml
$REPORTMERGE ./local-calligra/report.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml

cp -avf /tmp/report.xml .

xmlstarlet tr $report2htmlXSL -s expanded=1 report.xml >| report.html

if=report.html
sed -i 's.\\./.g' "$if"
sed -i 's|href="[^/]*/output/|href="output/|g' "$if"
sed -i 's|src="[^/]*/output/|src="output/|g' "$if"
sed -i 's|src="results/|src="output/|g' "$if"
sed -i 's|href="results/|src="output/|g' "$if"
