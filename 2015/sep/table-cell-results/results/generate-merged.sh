#!/bin/bash

REPORTMERGE=/Develop/officeshots/odfautoresults/report-merge.sh
report2htmlXSL=/Develop/officeshots/odfautotests/odfautotests/report2html.xsl

rm -f /tmp/report.xml
cp ./Word2016/report.xml  /tmp/report.xml
#$REPORTMERGE officeshots/odfautotests-libreoffice4-config.xml/report.xml /tmp/report.xml >| /tmp/reportB.xml
#mv -f /tmp/reportB.xml /tmp/report.xml

#cp officeshots/odfautotests-libreoffice5-config.xml/report.xml  /tmp/report.xml
$REPORTMERGE officeshots/odfautotests-libreoffice5-config.xml/report.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml

$REPORTMERGE officeshots/odfautotests-webodf-config.xml/report.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml

$REPORTMERGE officeshots/odfautotests-abiword-config.xml/report.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml

$REPORTMERGE officeshots/odfautotests-google-config.xml/report.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml

$REPORTMERGE config-calligra.xml/report.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml

cp -avf /tmp/report.xml .

xmlstarlet tr $report2htmlXSL -s expanded=1 report.xml >| report.html

if=report.html
sed -i 's.\\./.g' "$if"
sed -i 's|href="[^/]*/output/|href="output/|g' "$if"
sed -i 's|src="[^/]*/output/|src="output/|g' "$if"
sed -i 's|src="results/|src="output/|g' "$if"
sed -i 's|href="results/|src="output/|g' "$if"

rsync -av report.html report.xml ../
