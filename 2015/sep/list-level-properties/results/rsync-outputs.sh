#!/bin/bash

mkdir -p ../input
mkdir -p ../output

rsync -av ./config-calligra.xml/input  ..
for if in $(find . -name output); do
    echo $if
    rsync -av $if ..
done

cd ../output
/Develop/officeshots/odfautoresults/expand-odt.sh .
cd ../input
/Develop/officeshots/odfautoresults/expand-odt.sh .
cd -


