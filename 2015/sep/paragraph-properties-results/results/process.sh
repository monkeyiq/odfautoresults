#!/bin/bash
#
# You will have to change ODFAUTOTESTS below to suit your local environment.
#
#
ODFAUTOTESTS=/Develop/officeshots/odfautotests/odfautotests

SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
ODFTESTER="$ODFAUTOTESTS/odftester.jar"
TESTFILE="$ODFAUTOTESTS/tests/paragraph-properties-odt.xml"

CONFIGFILE=${1:?supply config path (relative to config dir) as arg1};

echo $CONFIGFILE
mkdir -p $CONFIGFILE
cd $CONFIGFILE

CFGDIR="$SCRIPTDIR/../../configs/"
java -jar $ODFTESTER -c "$CFGDIR/$CONFIGFILE" -i ./input -r ./output -t $TESTFILE
cd -;
