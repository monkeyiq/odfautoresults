#!/bin/bash

for f in ./sep/*/report.xml; do
    echo $f;
    xmlstarlet sel -N "t"="http://www.example.org/documenttests" -t \
	       -m '//t:testreport[t:input/t:validation/t:error]/@name' \
	       -v 'concat("  ",.)' -n $f;
done 2> /dev/null
