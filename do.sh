#!/bin/bash

BATCHNAME=${1:?Supply the location for the results as arg1. eg, /tmp/odftesting-2015nov};
export BATCHNAME

if [ -z $ODFAUTOTESTS ]; then
    echo "Please set ODFAUTOTESTS to the root of your odfautotests checkout"
    exit 1;
fi


export ODFTESTER="$ODFAUTOTESTS/odftester.jar"

cd ./scripts
STARTDIR=$(pwd);
STARTTIME=$(date);

TESTS=$(find . -type d -name "*properties");
for test in $TESTS; do
    echo "Running test $test"
    cd $STARTDIR/$test
    ./do.sh  "$BATCHNAME"
done

echo "Started   at: $STARTTIME"
echo "Completed at: $(date)"
